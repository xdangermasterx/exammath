#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

class Settings;
class QSpinBox;
class QTimeEdit;
class QCheckBox;
class QRadioButton;

class SettingsDialog : public QDialog
{
    Q_OBJECT
public:
    explicit SettingsDialog(QWidget *parent =nullptr);


signals:

    void settingsReceived(const Settings &settings);


private slots:

        void checkSigns();

private:



    QCheckBox *m_plusCB;
    QCheckBox *m_minusCB;
    QCheckBox *m_multypleCB;
    QCheckBox *m_devideCB;
    QRadioButton *m_onTimeRB;
    QRadioButton *m_onCountRB;
    QSpinBox *m_countSB;
    QSpinBox *m_minValSB;
    QSpinBox *m_maxValSB;
    QSpinBox *m_resultValSB;
    QTimeEdit *m_examTE;
    QPushButton *m_aplyBtn;

};

#endif // SETTINGSDIALOG_H
