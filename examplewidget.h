#ifndef EXAMPLEWIDGET_H
#define EXAMPLEWIDGET_H

#include <QWidget>

class Settings;
class QLCDNumber;
class QLabel;
class QPushButton;
class Example;
class QTimeEdit;
class QTimer;

class ExampleWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ExampleWidget(QWidget *parent = nullptr);
    ~ExampleWidget();

signals:


private slots:

    void generateValues();
    void handeleResult();

private:

    void resizeEvent(QResizeEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    int randomValue()const;
    char randomOperator(const QList <char>&operatorsList);
    void saveAnswer();

    void updateLeftTimer();

    int calcCountDigits(int val)const;

    QLCDNumber *fValDisplay;
    QLCDNumber *sValDisplay;
    QLCDNumber *aValDisplay;

    QPushButton *m_btn;


    Settings *m_settings;
    QLabel *m_operatorLbl;
    QLabel *m_eqLbl;

    QTimer *m_sampleTimer;
    QTimer *m_updateTimer;
    QTimeEdit *m_leftTimeEdit;

    QList<Example*>m_resultList;
    char m_currentOperator;

    bool settingsApplied;

};

#endif // EXAMPLEWIDGET_H
