#include "examplewidget.h"

#include <QDebug>
#include <QRandomGenerator>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QPushButton>
#include <QKeyEvent>
#include <QResizeEvent>
#include <QTimer>
#include <QTime>
#include <QTimeEdit>

#include "settings.h"
#include "example.h"
#include "settingsdialog.h"
#include "resultdialog.h"

ExampleWidget::ExampleWidget(QWidget *parent)
    : QWidget(parent)
{

    setWindowState(Qt::WindowMaximized);

    m_settings=new Settings;


    auto mainLay=new QVBoxLayout(this);
    auto sampleLay=new QHBoxLayout(this);

    fValDisplay=new QLCDNumber(this);
    sValDisplay=new QLCDNumber(this);
    aValDisplay=new QLCDNumber(this);
    m_leftTimeEdit=new QTimeEdit(this);

    m_btn=new QPushButton("Следующий",this);

    aValDisplay->setFocus();

    m_operatorLbl=new QLabel(this);
    m_eqLbl=new QLabel("=",this);

    m_updateTimer=new QTimer(this);
    m_sampleTimer=new QTimer(this);


    m_operatorLbl->setAlignment(Qt::AlignCenter);
    m_eqLbl->setAlignment(Qt::AlignCenter);
    m_leftTimeEdit->setDisplayFormat("hh:mm:ss");


    m_leftTimeEdit->setReadOnly(true);
    m_leftTimeEdit->setButtonSymbols(QTimeEdit::NoButtons);

    m_leftTimeEdit->setAlignment(Qt::AlignHCenter);


    sampleLay->addWidget(fValDisplay);
    sampleLay->addWidget(m_operatorLbl);
    sampleLay->addWidget(sValDisplay);
    sampleLay->addWidget(m_eqLbl);
    sampleLay->addWidget(aValDisplay);

    mainLay->addWidget(m_leftTimeEdit);
    mainLay->addLayout(sampleLay);
    mainLay->addWidget(m_btn);


    setLayout(mainLay);

    settingsApplied=false;


    connect(m_btn,&QPushButton::clicked,this,&ExampleWidget::handeleResult);

    SettingsDialog sd;


    connect(&sd,&SettingsDialog::settingsReceived,[this](const Settings &settings){

        m_settings->minVal=settings.minVal;
        m_settings->maxVal=settings.maxVal;
        m_settings->result=settings.result;
        m_settings->seconds=settings.seconds;
        m_settings->operatorsList=settings.operatorsList;

        fValDisplay->setDigitCount(calcCountDigits(settings.maxVal));
        sValDisplay->setDigitCount(calcCountDigits(settings.maxVal));
        aValDisplay->setDigitCount(calcCountDigits(settings.result));


        connect(m_sampleTimer,&QTimer::timeout,[this]{
            ResultDialog d;

            d.result(m_resultList);

            d.exec();
        });

        settingsApplied=true;
        generateValues();

        m_sampleTimer->start(m_settings->seconds*1000);

        updateLeftTimer();
    });

    sd.exec();


    connect(m_updateTimer,&QTimer::timeout,this,&ExampleWidget::updateLeftTimer);

    m_updateTimer->start(1000);

}

ExampleWidget::~ExampleWidget()
{
    for(auto &ex:m_resultList)
        delete ex;

    m_resultList.clear();

    delete m_settings;
}

void ExampleWidget::generateValues()
{

    const auto minVal=m_settings->minVal;
    const auto maxVal=m_settings->maxVal;
    const auto resVal=m_settings->result;

    m_currentOperator=randomOperator(m_settings->operatorsList);

    int fVal=0;
    int sVal=0;

    m_operatorLbl->setText(QChar(m_currentOperator));

    if(m_currentOperator=='+'){
        do{
            fVal=randomValue();
            sVal=randomValue();
        }while(fVal<minVal||fVal>maxVal
               ||sVal<minVal||sVal>maxVal
               ||fVal+sVal>resVal);
    }else if(m_currentOperator=='-'){
        do{
            fVal=randomValue();
            sVal=randomValue();
        }while(fVal<minVal||fVal>maxVal
               ||sVal<minVal||sVal>maxVal
               ||fVal-sVal>resVal
               ||fVal-sVal<0);
    }else if(m_currentOperator=='/'){
        do{
            fVal=randomValue();
            sVal=randomValue();
        }while(fVal<minVal||fVal>maxVal
               ||sVal<minVal||sVal>maxVal
               ||(sVal==0)||fVal%sVal);
    }else if(m_currentOperator=='*'){
        do{
            fVal=randomValue();
            sVal=randomValue();
        }while(fVal<minVal||fVal>maxVal
               ||sVal<minVal||sVal>maxVal
               ||fVal*sVal>resVal
               );
    }

    fValDisplay->display(fVal);
    sValDisplay->display(sVal);
    aValDisplay->display("");
}

void ExampleWidget::handeleResult()
{
    saveAnswer();
    generateValues();
}

void ExampleWidget::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);

    auto tmpFont=m_operatorLbl->font();

    tmpFont.setPointSize(event->size().height()/10);

    m_operatorLbl->setFont(tmpFont);
    m_eqLbl->setFont(tmpFont);

}

void ExampleWidget::keyPressEvent(QKeyEvent *event)
{
    if(!settingsApplied)
        return;

    if(event->key()!=Qt::Key_Enter && event->key()!=Qt::Key_Backspace && (event->key()<Qt::Key_0 || event->key()>Qt::Key_9)){
        return;
    }

    if(event->key()==Qt::Key_Enter){
        handeleResult();
        return;
    }


    const auto curVal=aValDisplay->intValue();
    const auto nextDig=event->text().toInt();

    if(event->key()==Qt::Key_Backspace)
        aValDisplay->display(curVal/10);
    else
        aValDisplay->display((10*curVal+nextDig)%static_cast<int>(pow(10,aValDisplay->digitCount())));

}

int ExampleWidget::randomValue() const
{
    const auto val=QRandomGenerator::global()->bounded(m_settings->minVal,m_settings->maxVal+1);

    return val;
}

char ExampleWidget::randomOperator(const QList<char> &operatorsList)
{
    const auto randIndex=QRandomGenerator::global()->bounded(operatorsList.size());

    return operatorsList.at(randIndex);
}

void ExampleWidget::saveAnswer()
{
    auto ex=new Example;

    ex->fVal=fValDisplay->intValue();
    ex->sVal=sValDisplay->intValue();
    ex->aVal=aValDisplay->intValue();
    ex->oper=m_currentOperator;

    switch (m_currentOperator) {
    case '+':
        ex->rVal=ex->fVal+ex->sVal;
        break;
    case '-':
        ex->rVal=ex->fVal-ex->sVal;
        break;
    case '*':
        ex->rVal=ex->fVal*ex->sVal;
        break;
    case '/':
        ex->rVal=ex->fVal/ex->sVal;
        break;
    default:
        break;
    }

    ex->isRight=(ex->rVal==ex->aVal);



    m_resultList.append(ex);


}

void ExampleWidget::updateLeftTimer()
{
    auto leftTimeS=m_sampleTimer->remainingTime()/1000;

    auto leftHours=leftTimeS/3600;
    auto leftMinutes=leftTimeS/60%60;
    auto leftSeconds=leftTimeS%60;
    m_leftTimeEdit->setTime(QTime(leftHours,leftMinutes,leftSeconds));
}

int ExampleWidget::calcCountDigits(int val) const
{
    int k=0;

    int tmpVal=val;

    while(tmpVal){
        k++;
        tmpVal/=10;
    }



    return k;
}
